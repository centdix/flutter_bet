const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

let moment = require('moment');

const db = admin.firestore();

exports.notifyOnUpload = functions.region('europe-west1').firestore
	 .document('betResults/{resultsId}')
	 .onUpdate((change, context) => {
		 const newValue = change.after.data();
		 const previousValue = change.before.data();

		 const results = newValue.results;
		 if (results === previousValue.results)
		 		return true;
		 db.collection('bets').doc(newValue.betId).get()
		 .then((betSnap) => {
		 		db.collection('users').doc(newValue.userId).get()
					.then((userSnap) => {
						let validated = false;
						if (betSnap.data().validation === "checkbox")
							validated = inputs[p][inputs[p].length - 1];
						else {
							let words = betSnap.data().validation.split(",");
							if (words[1] === "greater")
								validated = results[results.length - 1] > parseInt(words[2], 10);
							else
								validated = results[results.length - 1] < parseInt(words[2], 10);
						}
						if (validated) {
							let message = userSnap.data().email + " achieved his goal today !";
							betSnap.data().participants.forEach((p) => {
								if (p !== newValue.userId) {
									db.collection('users').doc(p).get()
									.then((otherSnap) => {
										let token = otherSnap.data().messageToken;
										const payload = {
												notification: {
														title: betSnap.data().name,
														body: message,
												}
										};
										if (token)
											admin.messaging().sendToDevice(token, payload);
											return true;
										}).catch((err) => {
												console.log(err)
										})
								}
							})
						}
						return true;
					}).catch((err) => {
							console.log(err)
					})
				return true;
		 }).catch((err) => {
				console.log(err)
			})
});

exports.dailyCheck = functions.region('europe-west1').pubsub.schedule('1 0 * * *')
.timeZone('Europe/Paris')
.onRun((context) => {
    let betsCollection = db.collection('bets');
    let resultsCollection = db.collection('betResults');
    let now = moment();
    betsCollection.get().then((betSnap) => {
        betSnap.forEach((bet) => {
            let startDate = moment(bet.data().startDate.toDate());
            let dayPassed = now.diff(startDate, 'days');
            if (now.diff(startDate) < 0)
                return false;
            if (dayPassed < 1) {
               betsCollection.doc(bet.id).update({status: "started"});
            }
            if (dayPassed > bet.data().duration)
                betsCollection.doc(bet.id).update({status: "finished"});
            let toShare = 0;
            let goodUsers = 0;
            let balances = {};
            let inputs = {};
            resultsCollection.where('betId', '==', bet.id).get().then((resultSnap) => {
                bet.data().participants.forEach((p) => {
                    let bet_results = resultSnap.docs.find((r) => r.data().userId === p);
										inputs[p] = [...bet_results.data().results];
										if (dayPassed < 1)
												balances[p] = -1 * (bet.data().amount / bet.data().participants.length);
										else
												balances[p] = bet_results.data().balance;
										if (inputs[p].length < dayPassed)
												bet.data().validation === "checkbox" ? inputs[p].push(false) : inputs[p].push(0);
                    let validated = false;
                    if (bet.data().validation === "checkbox")
                    	validated = inputs[p][inputs[p].length - 1];
                    else {
                    	let words = bet.data().validation.split(",");
                    	if (words[1] === "greater")
                      	validated = inputs[p][inputs[p].length - 1] > parseInt(words[2], 10);
                      else
                      	validated = inputs[p][inputs[p].length - 1] < parseInt(words[2], 10);
                    }
                    if (validated)
                    {
                        balances[p] += (bet.data().amount / bet.data().participants.length) / bet.data().duration;
                        goodUsers += 1;
                    }
                    else
                        toShare += (bet.data().amount / bet.data().participants.length) / bet.data().duration;
                })
                bet.data().participants.forEach((p) => {
                    let bet_results = resultSnap.docs.find((r) => r.data().userId === p);
                    let validated = false;
										if (bet.data().validation === "checkbox")
											validated = inputs[p][inputs[p].length - 1];
										else {
											let words = bet.data().validation.split(",");
											if (words[1] === "greater")
												validated = inputs[p][inputs[p].length - 1] > parseInt(words[2], 10);
											else
												validated = inputs[p][inputs[p].length - 1] < parseInt(words[2], 10);
										}
                    if (validated)
                        balances[p] += toShare / goodUsers;
                    else if (goodUsers === 0)
                    		balances[p] += (bet.data().amount / bet.data().participants.length) / bet.data().duration;
                    resultsCollection.doc(bet_results.id).update({balance: balances[p], results: inputs[p]});
                })
                return true;
            }).catch((err) => {
                console.log(err)
            })
        });
        return true;
    }).catch((err) => {
        console.log(err)
    })
    return false;
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
