import 'dart:async';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class BottomButton extends StatelessWidget {
  BottomButton({
    Key key,
    this.joined,
    this.started,
    this.betSnapshot,
    this.betResults
  }) : super(key: key);

  final bool joined;
  final bool started;
  final AsyncSnapshot<DocumentSnapshot> betSnapshot;
  final List<dynamic> betResults;
  final CollectionReference results =
  FirebaseFirestore.instance.collection('betResults');

  void _joinBet(DocumentReference betRef, List<dynamic> oldList) {
    results.add({
      'balance': null,
      'betId': betRef.id,
      'userId': auth.currentUser.uid,
      'results': [],
    });
    List<dynamic> newList = oldList;
    newList.add(auth.currentUser.uid);
    betRef
        .update({'participants': newList})
        .then((value) => print("Bet updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  void _leaveBet(DocumentReference betRef, List<dynamic> oldList) {
    results.where('betId', isEqualTo: this.betSnapshot.data.id).get()
    .then((value) {
      DocumentReference myResults = value.docs.firstWhere(
          (e) => e.data()['userId'] == auth.currentUser.uid).reference;
      results.doc(myResults.id).delete();
    });
    List<dynamic> newList = oldList;
    newList.remove(auth.currentUser.uid);
    betRef
        .update({'participants': newList})
        .then((value) => print("Bet updated"))
        .catchError((error) => print("Failed to update bet: $error"));
  }

  @override
  Widget build(BuildContext context) {
    final betRef = betSnapshot.data.reference;
    if (started == false) {
      return FlatButton(
        color: Colors.lightBlue,
        minWidth: double.infinity,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: BorderSide(color: Colors.blue)),
        onPressed: () {
          if (joined) {
            _leaveBet(betRef, betSnapshot.data.data()['participants']);
          } else {
            _joinBet(betRef, betSnapshot.data.data()['participants']);
          }
        },
        child: Text(joined ? "Leave bet" : "Join bet"),
      );
    }
    else {
      if (joined) {
        var myResults = this.betResults
            .firstWhere((e) => e.data()['userId'] == auth.currentUser.uid)
            .data()['results'];
        DateTime startDate = DateTime.fromMillisecondsSinceEpoch(
            betSnapshot.data.data()['startDate'].seconds * 1000);
        Duration duration = startDate.difference(DateTime.now());
        bool uploaded = myResults.length > duration.inDays.abs() ? true : false;
        return FlatButton(
          color: uploaded ? Colors.grey : Colors.lightBlue,
          minWidth: double.infinity,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.blue)),
          onPressed: () {
            if (uploaded == false) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return InputDialog(betSnapshot);
                  }
              );
            }
          },
          child: Text(uploaded ? "Already uploaded today" : "Upload result"),
        );
      } else {
        return SizedBox(
          width: 5,
        );
      }
    }
  }
}

class InputDialog extends StatefulWidget {
  InputDialog(this.betSnapshot);

  final AsyncSnapshot<DocumentSnapshot> betSnapshot;

  @override
  _InputDialogState createState() => _InputDialogState();
}

class _InputDialogState extends State<InputDialog> {
  final _inputController = TextEditingController();
  Widget _saveWidget = Text("Save");
  bool _checked = false;

  void _saveResult(BuildContext context, String input, bool checked) {
    setState(() {
      _saveWidget = CircularProgressIndicator(
        strokeWidth: 1,
      );
    });
    FirebaseFirestore.instance
        .collection('betResults')
        .where('betId', isEqualTo: widget.betSnapshot.data.id)
        .get()
        .then((QuerySnapshot querySnapshot) {
      QueryDocumentSnapshot result = querySnapshot.docs.firstWhere((e) {
        return (e.data()['userId'] == auth.currentUser.uid);
      });
      List<dynamic> inputs = result.data()['results'];
      if (widget.betSnapshot.data.data()['validation'] == "checkbox") {
        inputs.add(checked);
      }
      else {
        inputs.add(int.parse(input));
      }
      result.reference.update({'results': inputs}).then((value) {
        setState(() {
          _saveWidget = Icon(Icons.check_circle_outlined);
        });
        Navigator.pop(context);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: <Widget>[
        Container(
            height: 150,
            child: Column(
              children: [
                widget.betSnapshot.data.data()['validation'] == "checkbox" ?
                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  width: 200,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Checkbox(
                        value: _checked,
                        onChanged: (value) {
                          setState(() {
                            _checked = !_checked;
                          });
                        },
                      ),
                      Text(_checked ? "I achieved my goal today !"
                          : "I couldn't do it today..."),
                    ],
                  ),
                ) : Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  width: 200,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.blue,
                      ),
                      borderRadius: BorderRadius.circular(20)
                    ),
                  child: TextField(
                      keyboardType: TextInputType.number,
                      textAlignVertical: TextAlignVertical.center,
                      controller: _inputController,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        prefixIcon: Icon(Icons.add_chart),
                        hintText: "Steps",
                      ),
                    )
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FlatButton(
                      color: Colors.lightBlue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.black)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancel"),
                    ),
                    FlatButton(
                      color: Colors.lightGreen,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.black)),
                      onPressed: () {
                        _saveResult(context, _inputController.text, _checked);
                      },
                      child: _saveWidget,
                    ),
                  ],
                )
              ],
            ))
      ],
    );
  }
}

class BetPage extends StatefulWidget {
  BetPage(this.betId);
  final String betId;

  @override
  _BetPageState createState() => _BetPageState(this.betId);
}

class _BetPageState extends State<BetPage> {
  _BetPageState(this.betId);
  final String betId;

  StreamSubscription _betResultsSnap;
  List<dynamic> _betResults;

  @override
  void initState() {
    super.initState();
    listenToBetResults();
  }

  @override
  void dispose() {
    super.dispose();
    _betResultsSnap.cancel();
  }

  void listenToBetResults() {
    var snapshot = FirebaseFirestore.instance
        .collection('betResults')
        .where('betId', isEqualTo: this.betId)
        .snapshots();
    _betResultsSnap = snapshot.listen((e) => {});
    _betResultsSnap.onData((data) {
      setState(() {
        _betResults = data.docs;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    DocumentReference betRef =
        FirebaseFirestore.instance.collection('bets').doc(this.betId);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Bet info"),
        ),
        body: StreamBuilder(
            stream: betRef.snapshots(),
            builder: (BuildContext context,
                AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (snapshot.hasError) {
                return Text("Error");
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              }

              DateTime _startDate = DateTime.fromMillisecondsSinceEpoch(
                  snapshot.data.data()['startDate'].seconds * 1000);
              Duration _duration = _startDate.difference(DateTime.now());
              bool _joined = snapshot.data
                  .data()['participants']
                  .contains(auth.currentUser.uid);
              bool _started = _duration.isNegative;

              return Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  padding: EdgeInsets.all(10),
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 24),
                        child: Column(
                          children: [
                            Text(
                              snapshot.data.data()['name'],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 30),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(_duration.isNegative ?
                              "Started " + _duration.inHours.abs().toString() + " hours ago"
                              : "Starts in " + _duration.inHours.abs().toString() + " hours"),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Participants: ",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        height: 200,
                        width: double.infinity,
                        child: ListView.builder(
                            itemCount:
                                snapshot.data.data()['participants'].length,
                            itemBuilder: (BuildContext context, int index) {
                              DocumentReference userRef = FirebaseFirestore
                                  .instance
                                  .collection('users')
                                  .doc(snapshot.data.data()['participants']
                                      [index]);
                              return Container(
                                  margin: EdgeInsets.all(2),
                                  child: FutureBuilder(
                                      future: userRef.get(),
                                      builder: (BuildContext context,
                                          AsyncSnapshot<DocumentSnapshot>
                                              userSnapshot) {
                                        if (userSnapshot.hasError) {
                                          return Text("Error");
                                        }
                                        if (userSnapshot.connectionState ==
                                            ConnectionState.waiting) {
                                          return Center(child: CircularProgressIndicator());
                                        }

                                        QueryDocumentSnapshot result;
                                        if (_betResults != null && _started) {
                                          result = _betResults.firstWhere(
                                              (e) => e.data()['userId'] == snapshot.data.data()['participants'][index]);
                                        }
                                        return Container(
                                          margin: EdgeInsets.all(10),
                                          child: GestureDetector(
                                            onTap: () {
                                              if (_joined && _started) {
                                                var args = new Map();
                                                args['bet'] = snapshot.data.id;
                                                args['user'] =
                                                    userSnapshot.data.id;
                                                Navigator.pushNamed(
                                                    context, 'result',
                                                    arguments: args);
                                              } else {
                                                final snackBar = SnackBar(
                                                  content: Text(_started ?
                                                      'Only participants have access to this page'
                                                      : 'Bet has not started yet'),
                                                  duration:
                                                      Duration(seconds: 1),
                                                  action: SnackBarAction(
                                                    label: 'Close',
                                                    onPressed: () {
                                                      // Some code to undo the change.
                                                    },
                                                  ),
                                                );
                                                Scaffold.of(context).showSnackBar(snackBar);
                                              }
                                            },
                                            child: Row(
                                              children: [
                                                Expanded(
                                                    child: Text(userSnapshot.data.data()['email'])
                                                ),
                                                result != null ?
                                                  Expanded(
                                                    child: Text(result.data()['balance'].toString() + " €")
                                                  )
                                                  : Spacer(),
                                                Icon(Icons.arrow_forward_ios),
                                              ],
                                            ),
                                          ),
                                        );
                                      }));
                            }),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Rules: ",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        alignment: Alignment.center,
                        child: Text(
                          snapshot.data.data()['rules'],
                          style: TextStyle(fontSize: 14,),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Spacer(),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 15),
                        child: BottomButton(
                          joined: _joined,
                          started: _started,
                          betSnapshot: snapshot,
                          betResults: _betResults,
                        ),
                      )
                    ],
                  ));
            }));
  }
}
