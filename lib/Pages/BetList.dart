import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class BetList extends StatefulWidget {
  @override
  _BetListState createState() => _BetListState();
}

class _BetListState extends State<BetList> {
  CollectionReference bets = FirebaseFirestore.instance.collection('bets');

  void _signOut() async {
    await auth.signOut();
    Navigator.pushReplacementNamed(context, 'login');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Bets"),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: _signOut,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.pushNamed(context, 'addbet'),
      ),
      body: Column(
        children: [
          SizedBox(height: 20,),
          Text("Signed in as " + auth.currentUser.email),
          SizedBox(height: 10,),
          Center(
            child: StreamBuilder(
              stream: bets.snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError) {
                  return Text("Error");
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator(strokeWidth: 1,);
                }

                return Container(
                  height: 400,
                  child: ListView.builder(
                    itemCount: snapshot.data.docs.length,
                    itemBuilder: (BuildContext context, int index) {
                      DateTime _startDate = DateTime.fromMillisecondsSinceEpoch(snapshot.data.docs[index].data()['startDate'].seconds * 1000);
                      Duration _duration = _startDate.difference(DateTime.now());
                      return GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, 'bet',
                              arguments: snapshot.data.docs[index].id);
                        },
                        child: Container(
                          height: 100,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                            ),
                          ),
                          margin: EdgeInsets.all(20),
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                  children: [
                                    Text(snapshot.data.docs[index].data()['name'], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                                    SizedBox(height: 5,),
                                    Text(
                                        _duration.isNegative ?
                                        "Started " +  _duration.inHours.abs().toString() + " hours ago" :
                                        "Starts in " + _duration.inHours.abs().toString() + " hours"
                                    ),
                                  ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                              Row(children: [
                                Icon(Icons.person),
                                SizedBox(width: 8,),
                                Text(snapshot.data.docs[index].data()['participants'].length.toString())],
                              ),
                              Row(children: [
                                Icon(Icons.euro),
                                SizedBox(width: 8,),
                                Text(snapshot.data.docs[index].data()['amount'].toString())],
                              ),
                              Icon(Icons.arrow_forward_ios),
                            ],

                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            )
          ),
        ],
      )
    );
  }
}
