import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class AddBetPage extends StatefulWidget {
  @override
  _AddBetPageState createState() => _AddBetPageState();
}

class _AddBetPageState extends State<AddBetPage> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _rulesController = TextEditingController();
  final _amountController = TextEditingController();
  final _durationController = TextEditingController();
  final _numberController = TextEditingController();
  String _validation = "checkbox";
  String _numberComp = "greater";
  bool _showNumberInput = false;

  DateTime _date = DateTime.now().add(Duration(days: 1));
  Widget _saveWidget = Text("Save");
  String _stringDate = DateFormat('dd-MM-yyyy').format(DateTime.now().add(Duration(days: 1)));

  void _addBet(BuildContext context) {
    CollectionReference bets = FirebaseFirestore.instance.collection('bets');
    String validation = _validation != 'number' ? _validation
        : _validation + "," + _numberComp + "," + _numberController.text;
    bets.add({
      'name': _nameController.text,
      'rules': _rulesController.text,
      'amount': int.parse(_amountController.text),
      'duration': int.parse(_durationController.text),
      'participants': [],
      'validation': validation,
      'status': "waiting",
      'startDate': _date,
    }).then((value) {
      setState(() {
        _saveWidget = Icon(Icons.check_circle_outline);
      });
      Navigator.pop(context);
    }).catchError((error) => print("Failed to add bet: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add bet"),
        centerTitle: true,
      ),
      body: Form(
          key: _formKey,
          child: Container(
            margin: EdgeInsets.all(30),
            padding: EdgeInsets.all(12),
            child: SingleChildScrollView(
              child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    TextFormField(
                      controller: _nameController,
                      decoration: const InputDecoration(
                        hintText: 'Task Name',
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: _rulesController,
                      decoration: const InputDecoration(
                        hintText: 'Rules',
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: 6,
                      maxLength: 500,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: _amountController,
                      decoration: const InputDecoration(
                        hintText: 'Total amount',
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter a number';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: _durationController,
                      decoration: const InputDecoration(
                        hintText: 'Duration in days',
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter a number';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Container(
                          width: 150,
                          child: Text(
                            "Start date: ",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Container(
                          width: 110,
                          child: FlatButton(
                              color: Colors.lightBlue,
                              onPressed: () async {
                                _date = await showDatePicker(
                                  context: context,
                                  firstDate: DateTime.now(),
                                  lastDate: DateTime.now().add(Duration(days: 365)),
                                  initialDate: _date,
                                );
                                if (_date != null) {
                                  setState(() {
                                    _stringDate = DateFormat('dd-MM-yyyy').format(_date);
                                  });
                                }
                              },
                              child: Text(_stringDate)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Container(
                          width: 150,
                          child: Text(
                            "Validation type: ",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Container(
                          width: 110,
                          child: DropdownButton<String>(
                            value: _validation,
                            icon: Icon(Icons.arrow_downward, size: 15,),
                            isExpanded: true,
                            iconSize: 24,
                            elevation: 16,
                            style: TextStyle(color: Colors.deepPurple),
                            underline: Container(
                              height: 2,
                              color: Colors.deepPurpleAccent,
                            ),
                            onChanged: (String newValue) {
                              setState(() {
                                _validation = newValue;
                                newValue == "number" ? _showNumberInput = true : _showNumberInput = false;
                              });
                            },
                            items: <String>['checkbox', 'number']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                    Visibility(
                      visible: _showNumberInput,
                      child: Row(
                        children: [
                          Text("Number must be "),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            width: 60,
                            child: DropdownButton<String>(
                              value: _numberComp,
                              icon: Icon(Icons.arrow_downward, size: 15,),
                              isExpanded: true,
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  _numberComp = newValue;
                                });
                              },
                              items: <String>['greater', 'lower']
                                  .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                          Text(" than "),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            width: 100,
                            child: TextFormField(
                              controller: _numberController,
                              decoration: const InputDecoration(
                                hintText: 'number',
                                border: InputBorder.none
                              ),
                              keyboardType: TextInputType.number,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter a number';
                                }
                                return null;
                              },
                            ),
                          ),
                        ],
                      )
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              _saveWidget = CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              );
                            });
                            _addBet(context);
                          }
                        },
                        child: _saveWidget,
                      ),
                    ),
                  ]),
            ),
          )),
    );
  }
}
