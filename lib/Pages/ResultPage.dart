import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart' as charts;

FirebaseAuth auth = FirebaseAuth.instance;

class ResultsList extends StatelessWidget {
  const ResultsList({
    Key key,
    this.betId,
    this.results,
  }) : super(key: key);

  final String betId;
  final List<dynamic> results;

  @override
  Widget build(BuildContext context) {
    DocumentReference betRef = FirebaseFirestore.instance.collection('bets').doc(this.betId);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      height: 60,
      child: FutureBuilder(
          future: betRef.get(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> betSnap) {
            if (betSnap.hasError) {
              return Text("Error");
            }
            if (betSnap.connectionState == ConnectionState.waiting) {
              return CircularProgressIndicator(
                strokeWidth: 1,
              );
            }
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: this.results.length,
              itemBuilder: (BuildContext context, int index) {
                Timestamp timestamp = betSnap.data.data()['startDate'];
                DateTime date = new DateTime.fromMillisecondsSinceEpoch(
                        timestamp.seconds * 1000)
                    .add(Duration(days: index));
                String dateString = DateFormat('E dd/MM').format(date);
                return Container(
                  width: 110,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                        Row(
                          children: [
                            Icon(Icons.watch_later_outlined, size: 20,),
                            SizedBox(width: 5,),
                            Text(dateString),
                          ],
                        ),
                        SizedBox(height: 6,),
                        this.results[index].runtimeType == bool ?
                          this.results[index] == true ? Icon(Icons.check_circle, color: Colors.green,)
                            : Icon(Icons.cancel_rounded, color: Colors.red,)
                              : Text(this.results[index].toString())
                    ],
                  ),
                );
              },
            );
          }),
    );
  }
}

class StepsChart extends StatelessWidget {
  // final List<charts.Series> seriesList;
  final List<dynamic> results;
  final String betId;
  final bool animate;

  StepsChart(this.results, this.betId, this.animate);

  @override
  Widget build(BuildContext context) {
    DocumentReference betRef = FirebaseFirestore.instance.collection('bets').doc(this.betId);
    return FutureBuilder(
      future: betRef.get(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> betSnap) {
        if (betSnap.hasError) {
          return Text("Error");
        }
        if (betSnap.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator(
            strokeWidth: 1,
          );
        }

        Timestamp timestamp = betSnap.data.data()['startDate'];
        DateTime startDate = new DateTime.fromMillisecondsSinceEpoch(timestamp.seconds * 1000);
        return new charts.TimeSeriesChart(
          _createData(this.results, startDate),
          animate: animate,
          // Optionally pass in a [DateTimeFactory] used by the chart. The factory
          // should create the same type of [DateTime] as the data provided. If none
          // specified, the default creates local date time.
          dateTimeFactory: const charts.LocalDateTimeFactory(),
        );
      }
    );
  }

  List<charts.Series<StepSeries, DateTime>> _createData(List<dynamic> results, DateTime startDate) {
    final List<StepSeries> data = [];
    for (var index = 0; index < results.length; ++index) {
      var result = results[index];
      if (results[index].runtimeType == bool) {
        result = results[index] ? 1 : 0;
      }
      data.add(new StepSeries(startDate.add(Duration(days: index)), result));
    }

    return [
      new charts.Series<StepSeries, DateTime>(
        id: 'Steps',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (StepSeries steps, _) => steps.time,
        measureFn: (StepSeries steps, _) => steps.steps,
        data: data,
      )
    ];
  }
}

class StepSeries {
  final DateTime time;
  final int steps;

  StepSeries(this.time, this.steps);
}

class ResultPage extends StatelessWidget {
  ResultPage(this.args);

  final Map<dynamic, dynamic> args;

  @override
  Widget build(BuildContext context) {
    CollectionReference results =
        FirebaseFirestore.instance.collection('betResults');
    return FutureBuilder(
        future: results.where('betId', isEqualTo: this.args['bet']).get(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text("Error");
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator(
              strokeWidth: 1,
            );
          }

          QueryDocumentSnapshot query = snapshot.data.docs
              .firstWhere((e) => e.data()['userId'] == args['user']);
          DocumentReference userRef = FirebaseFirestore.instance
              .collection('users')
              .doc(query.data()['userId']);
          return Scaffold(
            appBar: AppBar(
              title: FutureBuilder(
                  future: userRef.get(),
                  builder: (BuildContext context,
                      AsyncSnapshot<DocumentSnapshot> userSnapshot) {
                    if (userSnapshot.hasError) {
                      return Text("Error");
                    }
                    if (userSnapshot.connectionState ==
                        ConnectionState.waiting) {
                      return CircularProgressIndicator(
                        strokeWidth: 1,
                      );
                    }
                    return Text(
                      userSnapshot.data.data()['email'],
                    );
                  }),
              centerTitle: true,
            ),
            body: Container(
              margin: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      width: 200,
                      height: 100,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Balance: ",
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            query.data()['balance'].toString() + "€",
                            style: TextStyle(fontSize: 25),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text("History: ", style: TextStyle(fontSize: 16),),
                  SizedBox(height: 6,),
                  ResultsList(
                      betId: this.args['bet'],
                      results: query.data()['results']
                  ),
                  Spacer(),
                  Text("Evolution: ", style: TextStyle(fontSize: 16),),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      height: 200,
                      width: double.infinity,
                      child: StepsChart(query.data()['results'], this.args['bet'], true),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
