import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatelessWidget {
  LoginPage(this.token);
  final String token;

  Future<UserCredential> _signIn(
      BuildContext context, String login, String pass) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: login, password: pass);
      Navigator.pushReplacementNamed(context, 'betlist');
      FirebaseFirestore.instance.collection('users')
          .doc(userCredential.user.uid)
          .update({
            'messageToken': this.token
          });
      return userCredential;
    } on FirebaseAuthException catch (e) {
      print(e);
    }
    return null;
  }

  Future<UserCredential> _signInWithGoogle(BuildContext context) async {
    // Trigger the authentication flow
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    UserCredential userCredential = await FirebaseAuth.instance.signInWithCredential(credential);
    FirebaseFirestore.instance.collection('users')
        .doc(userCredential.user.uid)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
            FirebaseFirestore.instance.collection('users').doc(userCredential.user.uid)
              .set({
                'email': userCredential.user.email,
                'messageToken': this.token
              });
        });
    Navigator.pushReplacementNamed(context, 'betlist');
    // Once signed in, return the UserCredential
    return (userCredential);
  }

  @override
  Widget build(BuildContext context) {

    final loginController = TextEditingController();
    final passwordController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text("Log in"),
        centerTitle: true,
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          width: 300,
          padding: EdgeInsets.symmetric(horizontal: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: Border.all(
                color: Colors.blue,
              ),
              borderRadius: BorderRadius.circular(20)),
          child: TextField(
            textAlignVertical: TextAlignVertical.center,
            controller: loginController,
            decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon: Icon(Icons.person),
              hintText: "Login",
            ),
          ),
        ),
        SizedBox(
          height: 12,
        ),
        Container(
          width: 300,
          padding: EdgeInsets.symmetric(horizontal: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: Border.all(
                color: Colors.blue,
              ),
              borderRadius: BorderRadius.circular(20)),
          child: TextField(
            textAlignVertical: TextAlignVertical.center,
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon: Icon(Icons.lock),
              hintText: "Password",
            ),
          ),
        ),
        SizedBox(
          height: 12,
        ),
        FlatButton(
          color: Colors.lightBlue,
          minWidth: 150,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.blue)),
          onPressed: () =>
              _signIn(context, loginController.text, passwordController.text),
          child: Text("Log in"),
        ),
        SizedBox(
          height: 18,
        ),
        Row(
          children: [
            Expanded(child: Divider(color: Colors.black, indent: 40, endIndent: 5,)),
            Text("OR", style: TextStyle(fontWeight: FontWeight.bold),),
            Expanded(child: Divider(color: Colors.black, indent: 5, endIndent: 40,)),
          ]
        ),
        SizedBox(
          height: 18,
        ),
        FlatButton(
          color: Colors.lightBlue,
          minWidth: 150,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.blue)),
          onPressed: () => _signInWithGoogle(context),
          child: Text("Log in with Google"),
        ),
        SizedBox(
          height: 18,
        ),
        GestureDetector(
          child: Text(
            "Don't have an account yet ? Sign up !",
            style: TextStyle(color: Colors.lightBlue),
          ),
          onTap: () {
            Navigator.pushNamed(context, '/signup');
          },
        ),
      ]),
    );
  }
}
