import 'package:flutter/material.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth auth = FirebaseAuth.instance;

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> with SingleTickerProviderStateMixin {
  AnimationController _playController;
  bool _playing = false;

  @override
  void initState() {
    super.initState();
    _playController = AnimationController(vsync: this, duration: Duration(milliseconds: 450));
  }

  @override
  void dispose() {
    super.dispose();
    _playController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
        child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          color: Colors.white,
          height: 100,
          width: 400,
          child: Center(child: Text("")),
        ),
        Container(
          color: Colors.white,
          height: 100,
          width: 400,
          child: Center(
              child: TextButton.icon(
                style: ButtonStyle(
                  minimumSize: MaterialStateProperty.all(Size(double.infinity, double.infinity)),
                  foregroundColor: _playing ?
                    MaterialStateProperty.all(Colors.red) :
                    MaterialStateProperty.all(Colors.blue),
                ),
                  onPressed: () {
                    setState(() {
                      _playing ? _playController.reverse() : _playController.forward();
                      _playing = !_playing;
                    });

                  },
                  icon: AnimatedIcon(
                    size: 55,
                    icon: AnimatedIcons.play_pause,
                    progress: _playController,
                  ),
                  label: Text(
                    _playing ? "STOP" : "START",
                    style: TextStyle(fontSize: 30),
                  )
              )),
        ),
        Container(
          color: Colors.white,
          height: 100,
          width: 400,
        ),
      ],
    ));
  }
}
