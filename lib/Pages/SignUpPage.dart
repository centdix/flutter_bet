import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignUpPage extends StatelessWidget {
  SignUpPage(this.token);
  final String token;

  Future<UserCredential> _signUp(BuildContext context, String login, String pass) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: login,
          password: pass
      );
      FirebaseFirestore.instance.collection('users')
          .doc(userCredential.user.uid)
          .get()
          .then((DocumentSnapshot documentSnapshot) {
        if (documentSnapshot.exists) {
          print('Document data: ${documentSnapshot.data()}');
        } else {
          FirebaseFirestore.instance.collection('users').doc(userCredential.user.uid)
              .set({
            'email': login,
            'password': pass,
            'messageToken': this.token
          });
        }
      });
      Navigator.pushReplacementNamed(context, 'betlist');
      return userCredential;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
      print(e);
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    final loginController = TextEditingController();
    final passwordController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign up"),
        centerTitle: true,
      ),
      body: Center(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 300,
              padding: EdgeInsets.symmetric(horizontal: 5),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.blue,
                  ),
                  borderRadius: BorderRadius.circular(20)
              ),
              child: TextField(
                textAlignVertical: TextAlignVertical.center,
                controller: loginController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  prefixIcon: Icon(Icons.person),
                  hintText: "Login",
                ),
              ),
            ),
            SizedBox(height: 12,),
            Container(
              width: 300,
              padding: EdgeInsets.symmetric(horizontal: 5),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.blue,
                  ),
                  borderRadius: BorderRadius.circular(20)
              ),
              child: TextField(
                textAlignVertical: TextAlignVertical.center,
                obscureText: true,
                controller: passwordController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  prefixIcon: Icon(Icons.lock),
                  hintText: "Password",
                ),
              ),
            ),
            SizedBox(height: 12,),
            FlatButton(
              color: Colors.lightBlue,
              minWidth: 150,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                  side: BorderSide(color: Colors.blue)
              ),
              onPressed: () => _signUp(context, loginController.text, passwordController.text),
              child: Text("Sign up"),
            ),
            SizedBox(height: 18,),
            GestureDetector(
              child: Text("Already have an account ? Log in",
                style: TextStyle(color: Colors.lightBlue),
              ),
              onTap: () {
                Navigator.pushNamed(context, 'login');
              },

            ),
          ]
      )),
    );
  }
}