// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import './Pages/LoginPage.dart';
import './Pages/SignUpPage.dart';
import './Pages/BetList.dart';
import './Pages/BetPage.dart';
import './Pages/ResultPage.dart';
import './Pages/AddBetPage.dart';


Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print(data);
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print(notification);
  }

  // Or do other work.
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final FirebaseAuth auth = FirebaseAuth.instance;
  String launchRoute = auth.currentUser != null ? 'betlist' : 'login';
  auth.authStateChanges().listen((User user) {
    if (user == null) {
      print('User is currently signed out!');
    } else {
      print('User is signed in!');
    }
  });
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print("onMessage: $message");
    },
    onBackgroundMessage: myBackgroundMessageHandler,
    onLaunch: (Map<String, dynamic> message) async {
      print("onLaunch: $message");
    },
    onResume: (Map<String, dynamic> message) async {
      print("onResume: $message");
    },
  );
  firebaseMessaging.getToken().then((token){
    runApp(MyApp(
        launchRoute: launchRoute,
        token: token
    ));
  });

}

class MyApp extends StatelessWidget {
  MyApp({
    this.launchRoute,
    this.token,
  });

  final String launchRoute;
  final String token;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wannabet',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: this.launchRoute,
      onGenerateRoute: (settings) {
        var routes = <String, WidgetBuilder>{
          'signup': (context) => SignUpPage(this.token),
          'login': (context) => LoginPage(this.token),
          'addbet': (context) => AddBetPage(),
          'betlist' : (context) => BetList(),
          'bet' : (context) => BetPage(settings.arguments),
          'result' : (context) => ResultPage(settings.arguments),
        };
        WidgetBuilder builder = routes[settings.name];
        return MaterialPageRoute(
          builder: (context) {
            return builder(context);
          },
        );
      }
    );
  }
}


